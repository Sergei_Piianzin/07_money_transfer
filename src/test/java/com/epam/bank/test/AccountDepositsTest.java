package com.epam.bank.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.epam.bank.account.Account;
import com.epam.bank.account.AccountFactory;
import com.epam.bank.account.AccountDeposits;

public class AccountDepositsTest {

  AccountFactory factory;
  List<Account> accounts = new ArrayList<>();
  AccountDeposits deposit;

  @Before
  public void init() {
    factory = new AccountFactory();
    accounts.add(factory.getInstance("Fox", 600L));
    accounts.add(factory.getInstance("White", 550L));
    accounts.add(factory.getInstance("Pinky", 1200L));
    deposit = new AccountDeposits(accounts);
  }

  @Test
  public void shouldCalculateTotalSum() {
    long sum = deposit.getSummaryBalance();
    assertThat(sum, is(2350L));
  }
}
