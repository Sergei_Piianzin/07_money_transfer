package com.epam.bank.app;

import com.epam.bank.account.AccountDeposits;
import com.epam.bank.account.AccountFactory;
import com.epam.bank.account.AccountsLoader;
import com.epam.bank.transaction.RandomTransactionService;
import com.epam.bank.writer.AccountFileWriter;

// TODO: Add tests and comments
public class App {
  public static void main(String[] args) {
    String root = "src/main/resources/accounts/";
    boolean write = false;
    if (write) {
      int accountsCount = 100;
      AccountFileWriter writer = new AccountFileWriter(root);
      AccountFactory factory = new AccountFactory();
      writer.writeAccount(factory.getRandomAccounts(accountsCount));
    }

    AccountsLoader loader = new AccountsLoader(root);
    AccountDeposits deposits = loader.getAccountDeposits();
    deposits.printAccounts();

    long transactionCount = 1000;
    int threadCount = 10;
    RandomTransactionService randomOperationService =
        new RandomTransactionService(threadCount, transactionCount, deposits);

    long oldTime = System.currentTimeMillis();
    randomOperationService.runOperations();
    long newTime = System.currentTimeMillis();

    long gap = newTime - oldTime;
    long transactionPerSec = transactionCount * 1000 / gap;
    deposits.printAccounts();
    System.out.println(transactionPerSec + " trans/sec");
  }
}
