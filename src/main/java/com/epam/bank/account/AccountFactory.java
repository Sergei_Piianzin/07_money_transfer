package com.epam.bank.account;

import java.util.ArrayList;
import java.util.List;

public class AccountFactory {
  public Account getInstance() {
    return new Account(IdService.getNewId());
  }

  public Account getInstance(String name, Long balance) {
    return new Account(IdService.getNewId(), name, balance);
  }

  public List<Account> getRandomAccounts(int count) {
    int maxMoney = 5000;
    List<Account> accounts = new ArrayList<>(count);
    for (int i = 0; i < count; ++i) {
      long balance = (long) (Math.random() * maxMoney);
      accounts.add(new Account(IdService.getNewId(), "random_account#" + i, balance));
    }
    return accounts;
  }
}


class IdService {
  private static long lastId = 0;

  public static long getNewId() {
    return lastId++;
  }
}
