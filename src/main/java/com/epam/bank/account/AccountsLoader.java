package com.epam.bank.account;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.epam.bank.reader.AccountFileReader;
import com.epam.bank.reader.FilesObserver;

public class AccountsLoader {
  private AccountFileReader reader;
  private AccountDeposits deposit;
  private FilesObserver observer;

  public AccountsLoader(String root) {
    observer = new FilesObserver(root);
    reader = new AccountFileReader();
    deposit = loadAccountsFromFiles();
  }

  private AccountDeposits loadAccountsFromFiles() {
    List<Path> paths = observer.getPaths();
    List<Account> accounts = new ArrayList<>(paths.size());
    for (Path p : paths) {
      Account account = reader.readAccount(p.toAbsolutePath().toString());
      accounts.add(account);
    }
    return new AccountDeposits(accounts);
  }

  public AccountDeposits getAccountDeposits() {
    return deposit;
  }
}
