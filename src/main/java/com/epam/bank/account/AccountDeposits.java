package com.epam.bank.account;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.bank.transaction.AccountDeficitException;
import com.epam.bank.transaction.NegativeCountException;
import com.epam.bank.transaction.SameAccountException;
import com.epam.bank.transaction.TransactionException;

public class AccountDeposits {
  private static final Logger logger = LoggerFactory.getLogger(AccountDeposits.class);
  private List<Account> accounts;
  private Boolean[] free;

  public AccountDeposits(List<Account> accounts) {
    this.accounts = accounts;
    free = new Boolean[accounts.size()];
    Arrays.fill(free, true);
  }

  public Long getSummaryBalance() {
    long sum = 0;
    for (Account a : accounts) {
      sum += a.getBalance();
    }
    return sum;
  }

  public List<Account> getAccounts() {
    return new ArrayList<>(accounts);
  }

  public void printAccounts() {
    for (Account a : accounts) {
      System.out.println(a);
    }
    System.out.printf("%nTotal: %d%n", getSummaryBalance());
  }

  public void remit(long fromId, long toId, long count) throws TransactionException {
    if (fromId == toId) {
      logger.info("Remit canceled: count {} fromId [{}] toId [{}] ", count, fromId, toId);
      throw new SameAccountException();
    }
    if (count <= 0) {
      logger.info("Remit canceled: count {} fromId [{}] toId [{}] ", count, fromId, toId);
      throw new NegativeCountException();
    } 
    int f = (int) fromId;
    int t = (int) toId;
    Account from = accounts.get(f);
    Account to = accounts.get(t);
    accountMinus(count, f, from);
    accountPlus(count, t, from, to);
    logger.debug("Remit: count {} fromId [{}] toId [{}]", count, fromId, toId);
  }

  private void accountPlus(long count, int t, Account from, Account to) {
    synchronized (to) {
      while (!free[t]) {
        try {
          from.wait();
        } catch (InterruptedException e) {
        }
      }
      free[t] = false;
      logger.trace("Account increase: {} plus {}", to, count);
      to.balancePlus(count);
      free[t] = true;
    }
  }

  private void accountMinus(long count, int f, Account from) throws AccountDeficitException {
    synchronized (from) {
      while (!free[f]) {
        try {
          from.wait();
        } catch (InterruptedException e) {
        }
      }
      if (from.getBalance() - count < 0) {
        throw new AccountDeficitException();
      }

      free[f] = false;
      logger.trace("Account decrease: {} minus {}", from, count);
      from.balanceMinus(count);
      free[f] = true;
    }
  }
}
