package com.epam.bank.account;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Account implements Serializable {
  private static final long serialVersionUID = 2548943030309184142L;
  private long id;
  private String name;
  private long balance;
  private LocalDateTime lastUsage;
  private boolean available;

  Account() {}

  Account(long id) {
    this.id = id;
    name = "old man";
    balance = 1000L;
    lastUsage = LocalDateTime.now();
    available = true;
  }

  Account(long id, String name, long balance) {
    this.id = id;
    this.name = name;
    this.balance = balance;
    lastUsage = LocalDateTime.now();
    available = true;
  }

  Account(long id, String name, long balance, boolean available, LocalDateTime lastUsage) {
    this.id = id;
    this.name = name;
    this.balance = balance;
    this.available = available;
    this.lastUsage = lastUsage;
  }

  void balancePlus(long plus) {
    balance += plus;
  }

  void balanceMinus(long minus) {
    balance -= minus;
  }

  long getBalance() {
    return balance;
  }
  
  public long getId() {
    return id;
  }

  @Override
  public String toString() {
    return String.format("id: %d; name: %s; balance: %d;", id, name, balance);
  }
}
