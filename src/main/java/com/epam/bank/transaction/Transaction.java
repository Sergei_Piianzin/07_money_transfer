package com.epam.bank.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.bank.account.AccountDeposits;

public class Transaction implements Runnable {
  private static final Logger logger = LoggerFactory.getLogger(Transaction.class);
  private long fromId;
  private long toId;
  private long count;
  private static AccountDeposits accountDeposits;

  public Transaction(AccountDeposits accountDeposits) {
    Transaction.accountDeposits = accountDeposits;
  }
  
  private Transaction(long fromId, long toId, long count) {
    this.fromId = fromId;
    this.toId = toId;
    this.count = count;
  }
  
  public Transaction getTransaction(long fromId, long toId, long count) {
    return new Transaction(fromId, toId, count);
  }

  public void run() {
    try {
      accountDeposits.remit(fromId, toId, count);
    } catch (TransactionException e) {
      logger.error("", e);
    }
  }
}
