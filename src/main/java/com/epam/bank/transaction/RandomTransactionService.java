package com.epam.bank.transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.epam.bank.account.AccountDeposits;

public class RandomTransactionService {
  private AccountDeposits accountDeposits;
  private long transactionCount;
  private int threadCount;

  public RandomTransactionService(int threadCount, long transactionsCount,
      AccountDeposits accountDeposits) {
    this.accountDeposits = accountDeposits;
    this.transactionCount = transactionsCount;
    this.threadCount = threadCount;
  }

  public void runOperations() {
    ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(threadCount);
    int accountsCount = accountDeposits.getAccounts().size();
    int maxMoneyValue = 5000;
    Transaction transactionFactory = new Transaction(accountDeposits);
    long transactionsDone = 0;
    while (transactionsDone < transactionCount) {
      long from = (long) (Math.random() * accountsCount);
      long to = (long) (Math.random() * accountsCount);
      long moneyCount = (long) (Math.random() * maxMoneyValue);
      Transaction transaction = transactionFactory.getTransaction(from, to, moneyCount);
      threadPoolExecutor.execute(transaction);
      transactionsDone++;
    }
    threadPoolExecutor.shutdown();
    try {
      threadPoolExecutor.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
    }
  }
}
