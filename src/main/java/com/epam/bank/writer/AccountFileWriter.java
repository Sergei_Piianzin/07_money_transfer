package com.epam.bank.writer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.bank.account.Account;

public class AccountFileWriter {
  private static final Logger logger = LoggerFactory.getLogger(AccountFileWriter.class);
  private String root;

  public AccountFileWriter(String root) {
    this.root = root;
  }

  public void writeAccount(Account account) {
    String pathToAccount = getPathToAccount(account);
    try (FileOutputStream outputStream = new FileOutputStream(pathToAccount);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
      objectOutputStream.writeObject(account);
      logger.info("Write account: {} in file {}\n", account.toString(), pathToAccount);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void writeAccount(List<Account> accounts) {
    for (int i = 0; i < accounts.size(); ++i) {
      writeAccount(accounts.get(i));
    }
  }
  
  private String getPathToAccount(Account account) {
    return root + account.getId() + ".acc"; 
  }

}
