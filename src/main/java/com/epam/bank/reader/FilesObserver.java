package com.epam.bank.reader;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilesObserver {
  private static final Logger logger = LoggerFactory.getLogger(FilesObserver.class);
  private String root;
  private List<Path> paths;

  public FilesObserver(String root) {
    this.root = root;
    paths = loadPaths();
  }

  private List<Path> loadPaths() {
    Path dir = Paths.get(root);
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
      List<Path> paths = new ArrayList<>();
      for (Path file : stream) {
        paths.add(file);
        logger.info(String.format("Add path: %s%n", file.toAbsolutePath().toString()));
      }
      return paths;
    } catch (IOException | DirectoryIteratorException x) {
      System.err.println(x);
    }
    return null;
  }

  public List<Path> getPaths() {
    return paths;
  }
}
