package com.epam.bank.reader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.bank.account.Account;

public class AccountFileReader {
  private static final Logger logger = LoggerFactory.getLogger(AccountFileReader.class);

  public AccountFileReader() {}

  public Account readAccount(String filePath) {
    try (FileInputStream inputStream = new FileInputStream(filePath);
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
      Account account = (Account) objectInputStream.readObject();
      logger.info("Read account: {}\n", account);
      return account;
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    return null;
  }

}
